package nl.livingit.booksapp;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = BooksappApplicationTests.class)
class BooksappApplicationTests {

	@Test
	void contextLoads() {

	}

}
