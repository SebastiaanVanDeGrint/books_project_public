INSERT INTO public.books(
	title, pages, genre, isbn, summary)
	VALUES  ('Harry Potter and the philosophers stone', 255, 'FANTASY', 123456789, 'Harry has a very exciting adventure');
INSERT INTO public.books(
    title, pages, genre, isbn, summary)
    VALUES ('The Clean Coder', 256, 'EDUCATIONAL',  9780137081073 , 'A Code of Conduct for Professional Programmers');
INSERT INTO public.books(
    title, pages, genre, isbn, summary)
    VALUES ('Clean Code',  464, 'EDUCATIONAL',   9780132350884  , 'A Handbook of Agile Software Craftsmanship');


-- ('ACTION', 'DETECTIVE', 'COMIC', 'EDUCATIONAL', 'FANTASY', 'FICTION', 'SCIFI', 'THRILLER', 'BIOGRAPHY', 'COOKBOOK');


INSERT INTO public.authors(name) VALUES ('Joanne Kathleen Rowling');
INSERT INTO public.authors(name) VALUES ('Robert Martin');

INSERT INTO public.books_authors(book_id, author_id) VALUES (1,1);
INSERT INTO public.books_authors(book_id, author_id) VALUES (2,2);
INSERT INTO public.books_authors(book_id, author_id) VALUES (3,2);

INSERT INTO public.owners(name) VALUES ('Sebastiaan van de Grint');
INSERT INTO public.owners(name) VALUES ('Jeff van Egmond');

INSERT INTO public.books_owners(book_id, owner_id) VALUES (1,1);
INSERT INTO public.books_owners(book_id, owner_id) VALUES (2,1);
INSERT INTO public.books_owners(book_id, owner_id) VALUES (2,2);
INSERT INTO public.books_owners(book_id, owner_id) VALUES (3,2);
INSERT INTO public.book_rating(book_id, owner_id, rating) VALUES (1,1,7.8);

INSERT INTO public.book_rating(book_id, owner_id, rating) VALUES (2,1,7.5);
INSERT INTO public.book_rating(book_id, owner_id, rating) VALUES (2,2,8.0);
INSERT INTO public.book_rating(book_id, owner_id, rating) VALUES (3,2,7.3);

INSERT INTO public.users(email, password, username) VALUES ('s.van.de.grint@livingit.nl', 'admin123', 'sebastiaan');
INSERT INTO public.users(email, password, username) VALUES ('j.van.egmond@livingit.nl', 'admin234', 'jeff');

INSERT INTO public.roles(name) VALUES('ROLE_USER');
INSERT INTO public.roles(name) VALUES('ROLE_ADMIN');

INSERT INTO public.user_roles(user_id, role_id) VALUES (1,1);
INSERT INTO public.user_roles(user_id, role_id) VALUES (1,2);
INSERT INTO public.user_roles(user_id, role_id) VALUES (2,1);
