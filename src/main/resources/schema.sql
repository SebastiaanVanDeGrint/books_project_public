-- Database: booksdb

-- DROP DATABASE IF EXISTS booksdb;
--
-- CREATE DATABASE booksdb
--     WITH
--     OWNER = root
--     ENCODING = 'UTF8'
--     LC_COLLATE = 'en_US.utf8'
--     LC_CTYPE = 'en_US.utf8'
--     TABLESPACE = pg_default
--     CONNECTION LIMIT = -1
--     IS_TEMPLATE = False;
DROP TYPE IF EXISTS genre CASCADE;

CREATE TYPE genre AS ENUM('ACTION','DETECTIVE','COMIC','EDUCATIONAL','FANTASY','FICTION','SCIFI','THRILLER','BIOGRAPHY','COOKBOOK');

DROP CAST IF EXISTS (character varying AS genre);

CREATE CAST (character varying AS genre) WITH INOUT AS IMPLICIT;

DROP TABLE IF EXISTS public.books_authors CASCADE;
DROP TABLE IF EXISTS public.books_owners;
DROP TABLE IF EXISTS public.book_rating;
DROP TABLE IF EXISTS public.user_roles CASCADE;

-- Table: public.books

DROP TABLE IF EXISTS public.books;

CREATE TABLE IF NOT EXISTS public.books
(
    id serial,
    title character varying(255) COLLATE pg_catalog."default" NOT NULL,
    pages bigint NOT NULL,
    genre genre,
    isbn character varying(20) COLLATE pg_catalog."default" NOT NULL,
    summary character varying(1023) COLLATE pg_catalog."default",
    CONSTRAINT books_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.books
    OWNER to root;

-- Table: public.authors

DROP TABLE IF EXISTS public.authors;

CREATE TABLE IF NOT EXISTS public.authors
(
    id serial NOT NULL,
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT authors_pkey PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.authors
    OWNER to root;

-- Table: public.books_authors

CREATE TABLE IF NOT EXISTS public.books_authors
(
    book_id bigint NOT NULL,
    author_id bigint NOT NULL,
    CONSTRAINT fk1b933slgixbjdslgwu888m34v FOREIGN KEY (book_id)
        REFERENCES public.books (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk3qua08pjd1ca1fe2x5cgohuu5 FOREIGN KEY (author_id)
        REFERENCES public.authors (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

    TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.books_authors
    OWNER to root;

-- Table: public.owners

DROP TABLE IF EXISTS public.owners;

CREATE TABLE IF NOT EXISTS public.owners
(
    id serial NOT NULL,
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT owners_pkey PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.owners
    OWNER to root;

-- Table: public.books_owners

CREATE TABLE IF NOT EXISTS public.books_owners
(
    book_id bigint NOT NULL,
    owner_id bigint NOT NULL,
    CONSTRAINT fknbebcm5k06emy1596yo1wjmmf FOREIGN KEY (owner_id)
        REFERENCES public.owners (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fkqjulltt0b1wodeb9dmius2w7p FOREIGN KEY (book_id)
        REFERENCES public.books (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

    TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.books_owners
    OWNER to root;

-- Table: public.book_rating

CREATE TABLE IF NOT EXISTS public.book_rating
(
    id serial,
    book_id bigint NOT NULL,
    owner_id bigint NOT NULL,
    rating NUMERIC (3, 1),
    CONSTRAINT book_rating_pkey PRIMARY KEY (id),
    CONSTRAINT book FOREIGN KEY (book_id)
        REFERENCES public.books (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT owner FOREIGN KEY (owner_id)
        REFERENCES public.owners (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

    TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.book_rating
    OWNER to root;

-- Table: public.users

DROP TABLE IF EXISTS public.users;

CREATE TABLE IF NOT EXISTS public.users
(
    id serial,
    email character varying(50) COLLATE pg_catalog."default",
    password character varying(120) COLLATE pg_catalog."default",
    username character varying(20) COLLATE pg_catalog."default",
    CONSTRAINT users_pkey PRIMARY KEY (id),
    CONSTRAINT uk6dotkott2kjsp8vw4d0m25fb7 UNIQUE (email),
    CONSTRAINT ukr43af9ap4edm43mmtq01oddj6 UNIQUE (username)
)

    TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.users
    OWNER to root;

-- Table: public.roles

DROP TABLE IF EXISTS public.roles;

CREATE TABLE IF NOT EXISTS public.roles
(
    id serial,
    name character varying(20) COLLATE pg_catalog."default",
    CONSTRAINT roles_pkey PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.roles
    OWNER to root;

-- Table: public.user_roles

CREATE TABLE IF NOT EXISTS public.user_roles
(
    user_id bigint NOT NULL,
    role_id integer NOT NULL,
    CONSTRAINT user_roles_pkey PRIMARY KEY (user_id, role_id),
    CONSTRAINT fkh8ciramu9cc9q3qcqiv4ue8a6 FOREIGN KEY (role_id)
        REFERENCES public.roles (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fkhfh9dx7w3ubf1co1vdev94g3f FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

    TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.user_roles
    OWNER to root;