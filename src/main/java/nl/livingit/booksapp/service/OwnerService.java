package nl.livingit.booksapp.service;

import nl.livingit.booksapp.model.Owner;

import java.util.List;
import java.util.Optional;

public interface OwnerService {
    List<Owner> findAll();
//    List<Owner> getAll();
    Optional<Owner> findById(Long ownerId);

    Owner save(Owner owner);

    void deleteById(Long ownerId);

}
