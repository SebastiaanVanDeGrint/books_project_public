package nl.livingit.booksapp.service;

import nl.livingit.booksapp.model.ExtraBookInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ExtraBookInfoServiceImpl {
    private RestTemplate template;

    @Autowired
    public ExtraBookInfoServiceImpl(RestTemplate template) {
        this.template = template;
    }

    public ExtraBookInfo findTitleByIsbn(String isbn){

        return template.getForObject("https://openlibrary.org/isbn/" + isbn + ".json", ExtraBookInfo.class);
//        return template.getForObject("https://openlibrary.org/isbn/9780140328721.json", ExtraBookInfo.class);

    }
}
