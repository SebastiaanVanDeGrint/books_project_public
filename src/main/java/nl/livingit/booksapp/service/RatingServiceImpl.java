package nl.livingit.booksapp.service;

import nl.livingit.booksapp.model.Rating;
import nl.livingit.booksapp.repository.RatingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class RatingServiceImpl implements RatingService{
    private RatingRepository ratingRepository;

    public RatingServiceImpl(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    @Override
    @Transactional
    public List<Rating> findAll() {
        return ratingRepository.findAll();
    }

    @Override
    @Transactional
    public Optional<Rating> findById(Long ratingId) {
        return ratingRepository.findById(ratingId);
    }

    @Override
    @Transactional
    public Rating save(Rating rating) {
        return ratingRepository.save(rating);
    }

    @Override
    @Transactional
    public void deleteById(Long ratingId) {
        ratingRepository.deleteById(ratingId);
    }
}
