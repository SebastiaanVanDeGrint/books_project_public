package nl.livingit.booksapp.service;

import nl.livingit.booksapp.model.Owner;
import nl.livingit.booksapp.repository.OwnerRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class OwnerServiceImpl implements OwnerService {

    private OwnerRepository ownerRepository;

    public OwnerServiceImpl(OwnerRepository ownerRepository) {
        this.ownerRepository = ownerRepository;
    }

    @Override
    @Transactional
    public List<Owner> findAll() {
        return ownerRepository.findAll();
    }

//    @Override
//    @Transactional
//    public List<Owner> getAll() {
//        return ownerRepository.findAll();
//    }

    @Override
    @Transactional
    public Optional<Owner> findById(Long ownerId) {
        return ownerRepository.findById(ownerId);
    }

    @Override
    @Transactional
    public Owner save(Owner owner) {
        return ownerRepository.save(owner);
    }

    @Override
    @Transactional
    public void deleteById(Long ownerId) {
        ownerRepository.deleteById(ownerId);
    }
}
