package nl.livingit.booksapp.service;

import nl.livingit.booksapp.model.Rating;

import java.util.List;
import java.util.Optional;

public interface RatingService {
    List<Rating> findAll();
    //    List<Owner> getAll();
    Optional<Rating> findById(Long ratingId);

    Rating save(Rating rating);

    void deleteById(Long ratingId);
}
