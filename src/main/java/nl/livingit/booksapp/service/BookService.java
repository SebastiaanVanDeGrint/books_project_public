package nl.livingit.booksapp.service;

import nl.livingit.booksapp.model.Book;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface BookService {
    List<Book> findAll();
    Optional<Book> findById(Long bookId);

//    <Book> getById();

    Book save(Book book);

    @Transactional
    void deleteById(Long bookId);

    List<Book> findByTitleContainsIgnoreCase(String title);
    List<Book> findByAuthors_Name(String name);

}
