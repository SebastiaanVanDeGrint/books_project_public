package nl.livingit.booksapp.service;

import nl.livingit.booksapp.model.Author;
import nl.livingit.booksapp.repository.AuthorRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorServiceImpl implements AuthorService{

    private AuthorRepository authorRepository;

    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    @Transactional
    public List<Author> findAll() {
        return authorRepository.findAll();
    }

    @Override
    @Transactional
    public List<Author> getAll() {
        return authorRepository.findAll();
    }


    @Override
    @Transactional
    public Optional<Author> findById(Long authorId) {
        return authorRepository.findById(authorId);
    }

    @Override
    @Transactional
    public Author save(Author author) {
        return authorRepository.save(author);
    }

    @Override
    @Transactional
    public void deleteById(Long authorId) {
        authorRepository.deleteById(authorId);
    }

    @Override
    @Transactional
    public List<Author> findByNameContainsIgnoreCase(String name) {
        return authorRepository.findByNameContainsIgnoreCase(name);
    }
}
