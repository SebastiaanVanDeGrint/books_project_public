package nl.livingit.booksapp.service;

import nl.livingit.booksapp.model.Book;
import nl.livingit.booksapp.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService{

    private BookRepository bookRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    @Transactional
    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    @Transactional
    public Optional<Book> findById(Long bookId) {
        return bookRepository.findById(bookId);
    }

    @Override
    @Transactional
    public Book save(Book book) {
        return bookRepository.save(book);
    }

    @Override
    @Transactional
    public void deleteById(Long bookId) {
        bookRepository.deleteById(bookId);
    }

    @Override
    public List<Book> findByTitleContainsIgnoreCase(String title) {
        return bookRepository.findByTitleContainsIgnoreCase(title);
    }

    @Override
    public List<Book> findByAuthors_Name(String name) {
        return bookRepository.findByAuthors_Name(name);
    }

}
