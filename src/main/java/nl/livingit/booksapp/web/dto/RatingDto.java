package nl.livingit.booksapp.web.dto;

public class RatingDto {
    private Long id;
    private Double rating;
    private BookDto book;
    private OwnerDto owner;

    public RatingDto() {
    }

    public RatingDto(Long id, Double rating, BookDto book, OwnerDto owner) {
        this.id = id;
        this.rating = rating;
        this.book = book;
        this.owner = owner;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public BookDto getBook() {
        return book;
    }

    public void setBook(BookDto book) {
        this.book = book;
    }

    public OwnerDto getOwner() {
        return owner;
    }

    public void setOwner(OwnerDto owner) {
        this.owner = owner;
    }
}
