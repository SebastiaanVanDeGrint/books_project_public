package nl.livingit.booksapp.web.dto;

import nl.livingit.booksapp.model.Genre;

import java.util.ArrayList;
import java.util.List;

public class BookDto {
    private Long id;
    private String title;
    private int pages;
    private Genre genre;
    private String isbn;
    private String summary;

    private List<AuthorDto> authors = new ArrayList<>();
    private List<OwnerDto> owners = new ArrayList<>();

    public BookDto() {
    }

    public BookDto(Long id, String title, int pages, Genre genre, String isbn, String summary, List<AuthorDto> authors,
                   List<OwnerDto> owners) {
        this.id = id;
        this.title = title;
        this.pages = pages;
        this.genre = genre;
        this.isbn = isbn;
        this.summary = summary;
        this.authors = authors;
        this.owners = owners;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<AuthorDto> getAuthors() {
        return authors;
    }

    public void addAuthor(AuthorDto author) {
        authors.add(author);
    }

    public List<OwnerDto> getOwners() {
        return owners;
    }

    public void addOwner(OwnerDto ownerDto) {
        owners.add(ownerDto);
    }

    //    private List<String> authorNames = new ArrayList<>();
}
