package nl.livingit.booksapp.web.controller;

import nl.livingit.booksapp.model.Owner;
import nl.livingit.booksapp.objectMapper.OwnerMapper;
import nl.livingit.booksapp.service.OwnerService;
import nl.livingit.booksapp.web.dto.OwnerDto;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@RestController
public class OwnerController {
    private OwnerService ownerService;
    private OwnerMapper ownerMapper;

    public OwnerController(OwnerService ownerService, OwnerMapper ownerMapper) {
        this.ownerService = ownerService;
        this.ownerMapper = ownerMapper;
    }

    @GetMapping("/owners")
    public List<OwnerDto> findAll(){
        return ownerService.findAll()
                .stream()
                .map(ownerMapper::toDto)
                .collect(toList());
    }

    @GetMapping("/owners/{ownerId}")
    public Optional<OwnerDto> getOwner(@PathVariable Long ownerId) {
        return ownerService.findById(ownerId)
                .stream()
                .map(ownerMapper::toDto)
                .findAny();
    }

    @PostMapping("/owners")
    public OwnerDto addOwner(@RequestBody OwnerDto ownerDto) {

        // this is to force a save of new item ... instead of update
        ownerDto.setId(0L);

        // convert  Dto to Entity
        Owner ownerRequest = ownerMapper.toOwner(ownerDto);

        Owner owner = ownerService.save(ownerRequest);

        // entity to Dto
        OwnerDto  ownerResponse = ownerMapper.toDto(owner);

        return ownerResponse;
    }

    @PutMapping("/owners/{ownerId}")
    public OwnerDto updateOwner(@RequestBody OwnerDto ownerDto, @PathVariable Long ownerId) {

        Optional<Owner> tempOwner = ownerService.findById(ownerId);
        if (tempOwner.isPresent()) {
            // convert  Dto to Entity
            Owner ownerRequest = ownerMapper.toOwner(ownerDto);

            Owner owner = ownerService.save(ownerRequest);

            // entity to Dto
            OwnerDto  ownerResponse = ownerMapper.toDto(owner);

            return ownerResponse;
        }
        //mogelijk exception toevoegen
        return null;
    }

    @DeleteMapping("/owners/{ownerId}")
    public String deleteOwner(@PathVariable Long ownerId) {

        Optional<Owner> tempOwner = ownerService.findById(ownerId);
        if (tempOwner.isPresent()) {
            ownerService.deleteById(ownerId);
        }
        return "Deleted owner with id - " + ownerId;
    }
}
