package nl.livingit.booksapp.web.controller;

import nl.livingit.booksapp.model.Author;
import nl.livingit.booksapp.objectMapper.AuthorMapper;
import nl.livingit.booksapp.service.AuthorService;
import nl.livingit.booksapp.web.dto.AuthorDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@RestController
public class AuthorController {
    private AuthorService authorService;
    private AuthorMapper authorMapper;

    @Autowired
    public AuthorController(AuthorService authorService, AuthorMapper authorMapper) {
        this.authorService = authorService;
        this.authorMapper = authorMapper;
     }

//    @GetMapping("/search-author")
//    public List<Author> findByNameContainsIgnoreCase(@RequestParam String name){
//        return authorService.findByNameContainsIgnoreCase(name);
//    }
    @GetMapping("/search-author")
    public List<AuthorDto> findByNameContainsIgnoreCase(@RequestParam String name){
        return authorService.findByNameContainsIgnoreCase(name)
                .stream()
                .map(authorMapper::toDto)
                .collect(toList());
    }

//    @GetMapping("/authors")
//    public List<Author> findAll(){
//        return authorService.findAll();
//    }

    @GetMapping("/authors")
    public List<AuthorDto> findAll(){
        return authorService.findAll()
                .stream()
                .map(authorMapper::toDto)
                .collect(toList());
    }

//    @GetMapping("/authors/{authorId}")
//    public Optional<Author> getAuthor(@PathVariable Long authorId) {
//        return authorService.findById(authorId);
//    }

    @GetMapping("/authors/{authorId}")
    public Optional<AuthorDto> getAuthor(@PathVariable Long authorId) {
        return authorService.findById(authorId)
                .stream()
                .map(authorMapper::toDto)
                .findAny();
    }

//    @PostMapping("/authors")
//    public Author addAuthor(@RequestBody Author theAuthor) {
//
//        // this is to force a save of new item ... instead of update
//
//        theAuthor.setId(0L);
//
//        authorService.save(theAuthor);
//
//        return theAuthor;
//    }
    @PostMapping("/authors")
    public AuthorDto addAuthor(@RequestBody AuthorDto authorDto) {

        // this is to force a save of new item ... instead of update
        authorDto.setId(0L);

        // convert  Dto to Entity
        Author authorRequest = authorMapper.toAuthor(authorDto);

        Author author = authorService.save(authorRequest);

        // entity to Dto
        AuthorDto  authorResponse = authorMapper.toDto(author);

        return authorResponse;
    }

//    @PutMapping("/authors/{authorId}")
//    public Author updateAuthor(@RequestBody Author theAuthor, @PathVariable Long authorId) {
//
//        Optional<Author> tempAuthor = authorService.findById(authorId);
//        if (tempAuthor.isPresent()) {
//            authorService.save(theAuthor);
//            return theAuthor;
//        }
//        //mogelijk exception toevoegen
//        return null;
//    }

    @PutMapping("/authors/{authorId}")
    public AuthorDto updateAuthor(@RequestBody AuthorDto authorDto, @PathVariable Long authorId) {

        Optional<Author> tempAuthor = authorService.findById(authorId);
        if (tempAuthor.isPresent()) {
            // convert  Dto to Entity
            Author authorRequest = authorMapper.toAuthor(authorDto);

            Author author = authorService.save(authorRequest);

            // entity to Dto
            AuthorDto  authorResponse = authorMapper.toDto(author);

            return authorResponse;
        }
        //mogelijk exception toevoegen
        return null;
    }

    @DeleteMapping("/authors/{authorId}")
    public String deleteAuthor(@PathVariable Long authorId) {

        Optional<Author> tempAuthor = authorService.findById(authorId);
        if (tempAuthor.isPresent()) {
            authorService.deleteById(authorId);
        }
        return "Deleted author with id - " + authorId;
    }
}
