package nl.livingit.booksapp.web.controller;

import nl.livingit.booksapp.model.ExtraBookInfo;
import nl.livingit.booksapp.service.ExtraBookInfoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExtraBookInfoController {

    private ExtraBookInfoServiceImpl extraBookInfoService;

    @Autowired
    public ExtraBookInfoController(ExtraBookInfoServiceImpl extraBookInfoService) {
        this.extraBookInfoService = extraBookInfoService;
    }

    @GetMapping("/titlebyisbn")
    public ExtraBookInfo findTitleByIsbn(@RequestParam("isbn") String isbn){
        return extraBookInfoService.findTitleByIsbn(isbn);
    }
}
