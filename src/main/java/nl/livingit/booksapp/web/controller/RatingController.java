package nl.livingit.booksapp.web.controller;

import nl.livingit.booksapp.model.Rating;
import nl.livingit.booksapp.objectMapper.RatingMapper;
import nl.livingit.booksapp.service.RatingService;
import nl.livingit.booksapp.web.dto.RatingDto;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@RestController
public class RatingController {
    private RatingService ratingService;
    private RatingMapper ratingMapper;

    public RatingController(RatingService ratingService, RatingMapper ratingMapper) {
        this.ratingService = ratingService;
        this.ratingMapper = ratingMapper;
    }

    @GetMapping("/ratings")
    public List<RatingDto> findAll(){
        return ratingService.findAll()
                .stream()
                .map(ratingMapper::toDto)
                .collect(toList());
    }

    @GetMapping("/ratings/{ratingId}")
    public Optional<RatingDto> getRating(@PathVariable Long ratingId) {
        return ratingService.findById(ratingId)
                .stream()
                .map(ratingMapper::toDto)
                .findAny();
    }

    @PostMapping("/ratings")
    public RatingDto addRating(@RequestBody RatingDto ratingDto) {

        // this is to force a save of new item ... instead of update
        ratingDto.setId(0L);

        // convert  Dto to Entity
        Rating ratingRequest = ratingMapper.toRating(ratingDto);

        Rating rating = ratingService.save(ratingRequest);

        // entity to Dto
        RatingDto  ratingResponse = ratingMapper.toDto(rating);

        return ratingResponse;
    }

    @PutMapping("/ratings/{ratingId}")
    public RatingDto updateRating(@RequestBody RatingDto ratingDto, @PathVariable Long ratingId) {

        Optional<Rating> tempRating = ratingService.findById(ratingId);
        if (tempRating.isPresent()) {
            // convert  Dto to Entity
            Rating ratingRequest = ratingMapper.toRating(ratingDto);

            Rating rating = ratingService.save(ratingRequest);

            // entity to Dto
            RatingDto  ratingResponse = ratingMapper.toDto(rating);

            return ratingResponse;
        }
        //mogelijk exception toevoegen
        return null;
    }

    @DeleteMapping("/ratings/{ratingId}")
    public String deleteRating(@PathVariable Long ratingId) {

        Optional<Rating> tempRating = ratingService.findById(ratingId);
        if (tempRating.isPresent()) {
            ratingService.deleteById(ratingId);
        }
        return "Deleted rating with id - " + ratingId;
    }
}
