package nl.livingit.booksapp.web.controller;

import nl.livingit.booksapp.model.Book;
import nl.livingit.booksapp.objectMapper.BookMapper;
import nl.livingit.booksapp.service.BookService;
import nl.livingit.booksapp.web.dto.BookDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@RestController
public class BookController {

    private BookService bookService;
    private BookMapper bookMapper;

    @Autowired
    public BookController(BookService bookService, BookMapper bookMapper) {
        this.bookService = bookService;
        this.bookMapper = bookMapper;
    }

//    @GetMapping("/search")
//    public List<Book> findByTitleContainsIgnoreCase(@RequestParam String title){
//        return bookService.findByTitleContainsIgnoreCase(title);
//    }

    @GetMapping("/search")
    public List<BookDto> findByTitleContainsIgnoreCase(@RequestParam String title){
        return bookService.findByTitleContainsIgnoreCase(title)
                .stream()
                .map(bookMapper::toDto)
                .collect(toList());
    }
//    @GetMapping("/search-book-by-author")
//    public List<Book> findByAuthors_Name(@RequestParam String name){
//        return bookService.findByAuthors_Name(name);
//    }
    @GetMapping("/search-book-by-author")
    public List<BookDto> findByAuthors_Name(@RequestParam String name){
        return bookService.findByAuthors_Name(name)
                .stream()
                .map(bookMapper::toDto)
                .collect(toList());
    }

//    @GetMapping("/books")
//    public List<Book> findAll(){
//        return bookService.findAll();
//    }
    @GetMapping("/books")
    public List<BookDto> findAll(){
        return bookService.findAll()
                .stream()
                .map(bookMapper::toDto)
                .collect(toList());
    }

    @GetMapping("/books/{bookId}")
    public Optional<BookDto> getBook(@PathVariable Long bookId) {
        return bookService.findById(bookId)
                .stream()
                .map(bookMapper::toDto)
                .findAny();
    }

//    @PostMapping("/books")
//    public Book addBook(@RequestBody Book theBook) {
//
//        // also just in case they pass an id in JSON ... set id to 0
//        // this is to force a save of new item ... instead of update
//
//        theBook.setId(0L);
//
//        bookService.save(theBook);
//
//        return theBook;
//    }
    @PostMapping("/books")
    public BookDto addBook(@RequestBody BookDto bookDto) {

        // also just in case they pass an id in JSON ... set id to 0
        // this is to force a save of new item ... instead of update
        bookDto.setId(0L);

        Book bookRequest = bookMapper.toBook(bookDto);

        Book book = bookService.save(bookRequest);

        BookDto bookResponse = bookMapper.toDto(book);

        return bookResponse;
    }

//    @PutMapping("/books/{bookId}")
//    public Book updateBook(@RequestBody Book theBook, @PathVariable Long bookId) {
//
//        Optional<Book> tempBook = bookService.findById(bookId);
//        if (tempBook.isPresent()) {
//            bookService.save(theBook);
//            return theBook;
//        }
//        //mogelijk exception toevoegen
//        return null;
//    }

    @PutMapping("/books/{bookId}")
    public BookDto updateBook(@RequestBody BookDto bookDto, @PathVariable Long bookId) {

        Optional<Book> tempBook = bookService.findById(bookId);
        if (tempBook.isPresent()) {

            Book bookRequest = bookMapper.toBook(bookDto);

            Book book = bookService.save(bookRequest);

            BookDto bookResponse = bookMapper.toDto(book);

            return bookResponse;
        }
        //mogelijk exception toevoegen
        return null;
    }

    @DeleteMapping("/books/{bookId}")
    public String deleteBook(@PathVariable Long bookId) {

        Optional<Book> tempBook = bookService.findById(bookId);
        if (tempBook.isPresent()) {
            bookService.deleteById(bookId);
        }
        return "Deleted book with id - " + bookId;
    }
}
