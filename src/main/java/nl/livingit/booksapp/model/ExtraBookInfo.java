package nl.livingit.booksapp.model;
public class ExtraBookInfo {
    private String publish_date;

    public ExtraBookInfo() {
    }

    public ExtraBookInfo(String publish_date) {
        this.publish_date = publish_date;
    }

    public String getPublish_date() {
        return publish_date;
    }

    public void setPublish_date(String publish_date) {
        this.publish_date = publish_date;
    }
}
