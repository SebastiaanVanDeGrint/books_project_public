package nl.livingit.booksapp.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Entity
@Table(name = "books")
public class Book {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String title;
    private int pages;
    @Enumerated(EnumType.STRING)
    private Genre genre;
    @Column(unique=true)
    private String isbn;
    private String summary;

//    testen of dit ook werkt in Dto's
//    @JsonIgnoreProperties("books")
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
        name = "books_authors",
        joinColumns = @JoinColumn(name = "book_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "author_id", referencedColumnName = "id"))
    private List<Author> authors = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "books_owners",
            joinColumns = @JoinColumn(name = "book_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "owner_id", referencedColumnName = "id"))
    private List<Owner> owners = new ArrayList<>();


    public Book() {
    }

    public Book(Long id, String title, int pages, Genre genre, String isbn, String summary, List<Author> authors,
        List<Owner> owners) {
        this.id = id;
        this.title = title;
        this.pages = pages;
        this.genre = genre;
        this.isbn = isbn;
        this.summary = summary;
        this.authors = authors;
        this.owners = owners;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getId() {
        return id;
    }
    public List<Author> getAuthors() {
        return Collections.unmodifiableList(authors);
    }

    public void addAuthor(Author author){
        authors.add(author);
    }

    public List<Owner> getOwners() {
        return owners;
    }

    public void addOwners(Owner owner) {
        owners.add(owner);
    }
}

