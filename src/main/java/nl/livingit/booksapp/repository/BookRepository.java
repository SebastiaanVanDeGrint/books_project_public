package nl.livingit.booksapp.repository;

import nl.livingit.booksapp.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    List<Book> findByTitleContainsIgnoreCase(String titleContains);
    List<Book> findByAuthors_Name(String name);
}
