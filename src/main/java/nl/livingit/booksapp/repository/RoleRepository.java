package nl.livingit.booksapp.repository;

import nl.livingit.booksapp.model.ERole;
import nl.livingit.booksapp.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}