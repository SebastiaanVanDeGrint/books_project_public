package nl.livingit.booksapp.objectMapper;

import nl.livingit.booksapp.model.Author;
import nl.livingit.booksapp.model.Book;
import nl.livingit.booksapp.web.dto.AuthorDto;
import nl.livingit.booksapp.web.dto.BookDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AuthorMapper {
    //check if it works with static methods
    public AuthorDto toDto(Author author){
        List<BookDto> books = new ArrayList<>();
        Long id = author.getId();
        String name = author.getName();
        for(Book book : author.getBooks()){
            BookDto bookDto = new BookDto();
            bookDto.setId(book.getId());
            bookDto.setGenre(book.getGenre());
            bookDto.setIsbn(book.getIsbn());
            bookDto.setTitle(book.getTitle());
            bookDto.setPages(book.getPages());
            bookDto.setSummary(book.getSummary());
            books.add(bookDto);
        }
        return new AuthorDto(id, name, books);
    }

    public Author toAuthor(AuthorDto authorDto){
        List<Book> books = new ArrayList<>();
        Long id = authorDto.getId();
        String name = authorDto.getName();
        for(BookDto bookDto : authorDto.getBooks()){
            Book book = new Book();
            book.setId(bookDto.getId());
            book.setGenre(bookDto.getGenre());
            book.setIsbn(bookDto.getIsbn());
            book.setTitle(bookDto.getTitle());
            book.setPages(bookDto.getPages());
            book.setSummary(bookDto.getSummary());
            books.add(book);
        }
        return new Author(id, name, books);
    }
}
