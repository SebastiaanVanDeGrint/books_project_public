package nl.livingit.booksapp.objectMapper;

import nl.livingit.booksapp.model.Author;
import nl.livingit.booksapp.model.Book;
import nl.livingit.booksapp.model.Genre;
import nl.livingit.booksapp.model.Owner;
import nl.livingit.booksapp.web.dto.AuthorDto;
import nl.livingit.booksapp.web.dto.BookDto;
import nl.livingit.booksapp.web.dto.OwnerDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BookMapper {
    public BookDto toDto(Book book){
        List<AuthorDto> authors = new ArrayList<>();
        List<OwnerDto> owners = new ArrayList<>();
        Long id = book.getId();
        String isbn = book.getIsbn();
        String title = book.getTitle();
        Genre genre = book.getGenre();
        int pages = book.getPages();
        String summary = book.getSummary();
        
        for(Author author : book.getAuthors()){
            AuthorDto authorDto = new AuthorDto();
            authorDto.setId(author.getId());
            authorDto.setName(author.getName());
            authors.add(authorDto);
        }
        for(Owner owner : book.getOwners()){
            OwnerDto ownerDto = new OwnerDto();
            ownerDto.setId(owner.getId());
            ownerDto.setName(owner.getName());
            owners.add(ownerDto);
        }

        return new BookDto(id, isbn, pages, genre, title, summary, authors, owners);
    }

    public Book toBook(BookDto bookDto){
        List<Author> authors = new ArrayList<>();
        List<Owner> owners = new ArrayList<>();
        Long id = bookDto.getId();
        String isbn = bookDto.getIsbn();
        String title = bookDto.getTitle();
        Genre genre = bookDto.getGenre();
        int pages = bookDto.getPages();
        String summary = bookDto.getSummary();

        for(AuthorDto authorDto : bookDto.getAuthors()){
            Author author = new Author();
            author.setId(authorDto.getId());
            author.setName(authorDto.getName());
            authors.add(author);
        }
        for(OwnerDto ownerDto : bookDto.getOwners()){
            Owner owner = new Owner();
            owner.setId(ownerDto.getId());
            owner.setName(ownerDto.getName());
            owners.add(owner);
        }
        return new Book(id, isbn, pages, genre, title, summary, authors, owners);
    }
}
