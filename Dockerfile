FROM amazoncorretto:11
ADD target/book-api-docker.jar book-api-docker.jar
EXPOSE 8080
EXPOSE 5005
ENV JAVA_TOOL_OPTIONS -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005
ENTRYPOINT ["java", "-jar","book-api-docker.jar"]
